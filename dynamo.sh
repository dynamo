#!/bin/bash
xset s off
xset -dpms
cd "$(dirname "$(readlink -e "$0")")"
while true
do
  killall -KILL pd
  sleep 1
  ./start.sh
  sleep 1
done &
wait
