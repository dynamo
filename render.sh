#!/bin/sh
for n in `seq 1 16`
do
  nn=`printf %02d $n`
  pd -noprefs -noaudio -r 44100 -batch -nrt -open loader.pd -send "load $n 1 ; pd dsp 1"
  flac --best --verify --silent --sector-align --delete-input-file --cuesheet=dynamo.pd.$nn.cue --tag="ARTIST=dynamo.pd" --tag="TITLE=dynamo.pd #$nn" dynamo.pd.$nn.wav
  flac --decode --silent dynamo.pd.$nn.flac
done
