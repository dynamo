LIBPD_DIR ?= ../../../

SRC_FILES = dynamo.c
TARGET = dynamo.html

CFLAGS = -I$(LIBPD_DIR)/pure-data/src -I$(LIBPD_DIR)/libpd_wrapper -O3
LDFLAGS = -L$(LIBPD_DIR)/libs -lpd -lm

.PHONY: clean clobber

$(TARGET): $(SRC_FILES) breakbeat.wav complex-mod~.pd hilbert~.pd main.pd Makefile
	emcc $(CFLAGS) -o $(TARGET) $(SRC_FILES) \
		-s USE_SDL=2 \
		-s 'EXPORTED_FUNCTIONS=["_main", "_pause", "_play"]' \
		-s 'EXTRA_EXPORTED_RUNTIME_METHODS=["ccall", "cwrap"]' \
		--preload-file breakbeat.wav \
		--preload-file complex-mod~.pd \
		--preload-file hilbert~.pd \
		--preload-file main.pd \
		$(LDFLAGS)
